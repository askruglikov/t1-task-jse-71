package ru.t1.kruglikov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.TaskDTO;
import ru.t1.kruglikov.tm.enumerated.Status;
import ru.t1.kruglikov.tm.enumerated.TaskSort;

import java.util.List;

public interface ITaskDtoService extends IUserOwnedDtoService<TaskDTO> {

    @NotNull
    List<TaskDTO> findAll(
            @Nullable final String userId,
            @Nullable final TaskSort sort
    );

    @NotNull
    List<TaskDTO> findAll(
            @Nullable final TaskSort sort
    );

    @NotNull
    TaskDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO updateByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    @NotNull
    TaskDTO changeTaskStatusByIndex(
            @Nullable String userId,
            @Nullable Integer index,
            @Nullable Status status
    );

    @NotNull
    List<TaskDTO> findAllByProjectId(
            @Nullable String userId,
            @Nullable String projectId
    );

    void bindTaskToProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    void unbindTaskFromProject(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String taskId
    );

    void removeProjectById(
            @Nullable String userId,
            @Nullable String projectId
    );

}
