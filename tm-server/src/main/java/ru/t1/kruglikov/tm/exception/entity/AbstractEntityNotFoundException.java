package ru.t1.kruglikov.tm.exception.entity;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.kruglikov.tm.exception.AbstractException;

@NoArgsConstructor
public abstract class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException(@NotNull final String message) {
        super(message);
    }

    public AbstractEntityNotFoundException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractEntityNotFoundException(
            @NotNull final String message,
            @NotNull final Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractEntityNotFoundException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
