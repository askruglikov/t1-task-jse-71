package ru.t1.kruglikov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.kruglikov.tm.dto.model.TaskDTO;
import ru.t1.kruglikov.tm.enumerated.TaskSort;
import ru.t1.kruglikov.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Nullable
    @Query("SELECT p FROM TaskDTO p WHERE p.userId = :userId ORDER BY :sort")
    List<Task> findAll(
            @NotNull String userId,
            @NotNull String sort
    );

    @Nullable
    @Query("SELECT p FROM TaskDTO p ORDER BY :sort")
    List<Task> findAll(
            @NotNull String sort
    );

}