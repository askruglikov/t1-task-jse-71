package ru.t1.kruglikov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.kruglikov.tm.dto.model.SessionDTO;
import ru.t1.kruglikov.tm.enumerated.SessionSort;
import ru.t1.kruglikov.tm.model.Session;

import java.util.List;

@Repository
@Scope("prototype")
public interface ISessionRepository extends IUserOwnedRepository<Session> {

    @Nullable
    @Query("SELECT p FROM SessionDTO p ORDER BY :sort")
    List<Session> findAll(@Nullable String sort);

}

