package ru.t1.kruglikov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.t1.kruglikov.tm.enumerated.ProjectSort;
import ru.t1.kruglikov.tm.model.Project;

import java.util.List;

@Repository
@Scope("prototype")
public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Nullable
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY :sort")
    List<Project> findAll(
            @NotNull String userId,
            @NotNull String sort
    );

    @Nullable
    @Query("SELECT p FROM ProjectDTO p ORDER BY :sort")
    List<Project> findAll(
            @NotNull String sort
    );

}
