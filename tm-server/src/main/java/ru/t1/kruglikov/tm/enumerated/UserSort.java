package ru.t1.kruglikov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum UserSort {

    BY_LOGIN("Sort by login", "LOGIN"),
    BY_FIRST_NAME("Sort by first name", "FIRST_NAME"),
    BY_LAST_NAME("Sort by last name", "LAST_NAME");


    @NotNull
    private final String name;

    @NotNull
    private final String columnName;

    UserSort(
            @NotNull final String name,
            @NotNull final String columnName
    ) {
        this.name = name;
        this.columnName = columnName;
    }

    @Nullable
    public static UserSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final UserSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }

}
