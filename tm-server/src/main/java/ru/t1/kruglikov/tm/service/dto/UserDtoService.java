package ru.t1.kruglikov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.kruglikov.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.kruglikov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.kruglikov.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.kruglikov.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.kruglikov.tm.api.service.IConnectionService;
import ru.t1.kruglikov.tm.api.service.IPropertyService;
import ru.t1.kruglikov.tm.api.service.dto.IProjectDtoService;
import ru.t1.kruglikov.tm.api.service.dto.ISessionDtoService;
import ru.t1.kruglikov.tm.api.service.dto.ITaskDtoService;
import ru.t1.kruglikov.tm.api.service.dto.IUserDtoService;
import ru.t1.kruglikov.tm.dto.model.UserDTO;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.enumerated.UserSort;
import ru.t1.kruglikov.tm.exception.entity.EntityNotFoundException;
import ru.t1.kruglikov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kruglikov.tm.exception.entity.UserNotFoundException;
import ru.t1.kruglikov.tm.exception.field.EmailEmptyException;
import ru.t1.kruglikov.tm.exception.field.IdEmptyException;
import ru.t1.kruglikov.tm.exception.field.LoginEmptyException;
import ru.t1.kruglikov.tm.exception.field.PasswordEmptyException;
import ru.t1.kruglikov.tm.exception.user.ExistsEmailException;
import ru.t1.kruglikov.tm.exception.user.ExistsLoginException;
import ru.t1.kruglikov.tm.exception.user.RoleEmptyException;
import ru.t1.kruglikov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class UserDtoService extends AbstractDtoService<UserDTO, IUserDtoRepository> implements IUserDtoService {

    @NotNull
    @Autowired
    private  IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserDtoRepository userDtoRepository;

    @NotNull
    @Autowired
    private ITaskDtoService taskDtoService;

    @NotNull
    @Autowired
    private IProjectDtoService projectDtoService;

    @NotNull
    @Autowired
    private ISessionDtoService sessionDtoService;

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return add(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        return add(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        return add(user);
    }

    @Nullable
    @Override
    public List<UserDTO> findAll(@Nullable UserSort sort) {
        if (sort == null) return userDtoRepository.findAll();
        return userDtoRepository.findAll(sort.getColumnName());
    };

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final Optional <UserDTO> found = userDtoRepository.findAll()
                .stream()
                .filter(x -> x.getLogin().equals(login)).findFirst();
        return found.orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final Optional <UserDTO> found = userDtoRepository.findAll()
                .stream()
                .filter(x -> x.getEmail() != null && x.getEmail().equals(email))
                .findFirst();
        return found.orElse(null);
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO removeOne(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        taskDtoService.removeAll(user.getId());
        projectDtoService.removeAll(user.getId());
        sessionDtoService.removeAll(user.getId());
        userDtoRepository.deleteById(user.getId());
        return user;
    }

    @Override
    @Transactional
    public void removeAll() {
        taskDtoService.removeAll();
        projectDtoService.removeAll();
        sessionDtoService.removeAll();
        userDtoRepository.deleteAll();
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO removeOneByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();

        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new EntityNotFoundException();
        return removeOne(user);
    }

    @Nullable
    @Override
    @Transactional
    public UserDTO removeOneByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();

        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) throw new EntityNotFoundException();
        return removeOne(user);
    }

    @NotNull
    @Transactional
    private UserDTO update(@Nullable final UserDTO user) {
        if (user == null) throw new ProjectNotFoundException();
        userDtoRepository.save(user);
        return user;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();

        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new EntityNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));

        return update(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new EntityNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);

        return update(user);
    }

    @NotNull
    @Transactional
    private UserDTO setLockOneByLogin(
            @Nullable final String login,
            @NotNull final Boolean locked
    ) {
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(locked);

        return update(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO lockOneByLogin(@Nullable final String login) {
        return setLockOneByLogin(login, true);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO unlockOneByLogin(@Nullable final String login) {
        return setLockOneByLogin(login, false);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

}
