package ru.t1.kruglikov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.kruglikov.tm.dto.model.ProjectDTO;
import ru.t1.kruglikov.tm.enumerated.ProjectSort;

import java.util.List;

@Repository
@Scope("prototype")
public interface IProjectDtoRepository extends IUserOwnedDtoRepository<ProjectDTO> {

    @Nullable
    @Query("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY :sort")
    List<ProjectDTO> findAll(
            @NotNull String userId,
            @NotNull String sort
    );

    @Nullable
    @Query("SELECT p FROM ProjectDTO p ORDER BY :sort")
    List<ProjectDTO> findAll(
            @NotNull String sort
    );

}

