package ru.t1.kruglikov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.kruglikov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.kruglikov.tm.api.service.IConnectionService;
import ru.t1.kruglikov.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.kruglikov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.kruglikov.tm.exception.entity.EntityNotFoundException;
import ru.t1.kruglikov.tm.exception.field.IdEmptyException;
import ru.t1.kruglikov.tm.exception.field.IndexIncorrectException;
import ru.t1.kruglikov.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedDtoRepository<M>>
        extends AbstractDtoService<M, R> implements IUserOwnedDtoService<M> {

    @NotNull
    @Autowired
    protected IUserOwnedDtoRepository<M> repository;

    @Nullable
    @Override
    @Transactional
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        repository.save(model);
        return model;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll()
                .stream()
                .filter(x -> x.getUserId().equals(userId)).collect(Collectors.toList());
    }

    @Nullable
    @Override
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Optional<M> found = repository.findAll()
                .stream()
                .filter(x -> x.getId().equals(id) && x.getUserId().equals(userId))
                .findFirst();
        return found.orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @NotNull final List<M> models = findAll(userId);
        return models.get(index);
    }

    @Override
    @Transactional
    public M removeOne(
            @Nullable final String userId,
            @Nullable final M model
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        repository.findAll()
                .stream()
                .filter(x -> x.getUserId().equals(userId))
                .forEach(x -> repository.delete(x));
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public M removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @Nullable M model = findOneById(userId, id);
        if (model == null) return null;

        return removeOne(model);
    }

    @Nullable
    @Override
    @Transactional
    public M removeOneByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();

        @Nullable M model = findOneByIndex(userId, index);
        if (model == null) return null;

        return removeOne(model);
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.findAll()
                .stream()
                .filter(x -> x.getUserId().equals(userId))
                .forEach(x -> repository.delete(x));
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll()
                .stream()
                .filter(x -> x.getUserId().equals(userId))
                .count();
    }

    @Override
    public boolean existsById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;

        return findOneById(userId, id) != null;
    }

}