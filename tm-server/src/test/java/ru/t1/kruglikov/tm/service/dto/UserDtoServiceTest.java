package ru.t1.kruglikov.tm.service.dto;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import ru.t1.kruglikov.tm.api.service.IConnectionService;
import ru.t1.kruglikov.tm.api.service.IPropertyService;
import ru.t1.kruglikov.tm.api.service.dto.IProjectDtoService;
import ru.t1.kruglikov.tm.api.service.dto.ITaskDtoService;
import ru.t1.kruglikov.tm.api.service.dto.IUserDtoService;
import ru.t1.kruglikov.tm.dto.model.UserDTO;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.enumerated.SessionSort;
import ru.t1.kruglikov.tm.enumerated.UserSort;
import ru.t1.kruglikov.tm.exception.entity.EntityNotFoundException;
import ru.t1.kruglikov.tm.exception.entity.UserNotFoundException;
import ru.t1.kruglikov.tm.exception.entity.ValueIsNullException;
import ru.t1.kruglikov.tm.exception.field.*;
import ru.t1.kruglikov.tm.exception.user.ExistsEmailException;
import ru.t1.kruglikov.tm.exception.user.ExistsLoginException;
import ru.t1.kruglikov.tm.exception.user.RoleEmptyException;
import ru.t1.kruglikov.tm.marker.UnitCategory;
import ru.t1.kruglikov.tm.migration.AbstractSchemeTest;
import ru.t1.kruglikov.tm.service.ConnectionService;
import ru.t1.kruglikov.tm.service.PropertyService;
import ru.t1.kruglikov.tm.util.HashUtil;

import java.util.List;

import static ru.t1.kruglikov.tm.constant.dto.UserDtoTestData.*;

@Category(UnitCategory.class)
public final class UserDtoServiceTest extends AbstractSchemeTest {

//    @Autowired
//    @NotNull
//    private IPropertyService propertyService;
//
//    @NotNull
//    @Autowired
//    private IConnectionService connectionService;
//
//    @NotNull
//    @Autowired
//    private IProjectDtoService projectService;
//
//    @NotNull
//    @Autowired
//    private ITaskDtoService taskService;
//
//    @NotNull
//    @Autowired
//    private IUserDtoService userService;
//
//    @Rule
//    public ExpectedException thrown = ExpectedException.none();
//
//    @Before
//    public void beforeTest() throws LiquibaseException {
//        final Liquibase liquibase = liquibase("changelog/changelog.xml");
//        liquibase.dropAll();
//        liquibase.update("scheme");
//        userService.removeAll();
//        projectService.removeAll();
//        taskService.removeAll();
//    }
//
//    @After
//    public void clearUsers() {
//        userService.removeAll();
//    }
//
//    @Test
//    public void testUserAdd() {
//        Assert.assertNotNull(userService.add(USER1));
//        Assert.assertEquals(USER1.getId(), userService.findAll().get(0).getId());
//
//        thrown.expect(EntityNotFoundException.class);
//        userService.add(NULL_USER);
//    }
//
//    @Test
//    public void testUserAddModels() {
//        final UserSort sort = UserSort.BY_LOGIN;
//        Assert.assertNotNull(userService.add(USER_LIST2));
//        Assert.assertEquals(USER_LIST2.get(0).getId(), userService.findAll(sort).get(0).getId());
//
//        thrown.expect(ValueIsNullException.class);
//        userService.add(NULL_USER_LIST);
//    }
//
//    @Test
//    public void testUserSet() {
//        final UserSort sort = UserSort.BY_LOGIN;
//        Assert.assertNotNull(userService.set(USER_LIST2));
//        Assert.assertEquals(USER_LIST2.get(0).getId(), userService.findAll(sort).get(0).getId());
//        userService.set(ADMIN_LIST);
//        Assert.assertEquals(ADMIN_LIST.get(0).getId(), userService.findAll(sort).get(0).getId());
//
//        thrown.expect(ValueIsNullException.class);
//        userService.add(NULL_USER_LIST);
//    }
//
//    @Test
//    public void testUserFindAll() {
//        final UserSort sort = UserSort.BY_LOGIN;
//        userService.add(USER_LIST2);
//        @NotNull final List<UserDTO> userList = userService.findAll(sort);
//        Assert.assertEquals(USER_LIST2.get(0).getId(), userList.get(0).getId());
//    }
//
//    @Test
//    public void testUserFindOneById() {
//        userService.add(USER_LIST);
//        Assert.assertEquals(USER1.getId(), userService.findOneById(USER1.getId()).getId());
//        Assert.assertNull(userService.findOneById("test-id"));
//
//        thrown.expect(IdEmptyException.class);
//        userService.findOneById(NULL_USER_ID);
//    }
//
//    @Test
//    public void testUserFindOneByIndex() {
//        userService.add(USER_LIST2);
//        Assert.assertEquals(USER1.getId(), userService.findOneByIndex(0).getId());
//
//        thrown.expect(IndexIncorrectException.class);
//        userService.findOneByIndex(-1);
//    }
//
//    @Test
//    public void testUserRemoveOne() {
//        userService.add(USER_LIST);
//        Assert.assertNotNull(userService.findOneById(USER1.getId()));
//        Assert.assertNotNull(userService.removeOne(USER1));
//        Assert.assertNull(userService.findOneById(USER1.getId()));
//    }
//
//    @Test
//    public void testUserRemoveOneById() {
//        userService.add(USER1);
//        Assert.assertNotNull(userService.findOneById(USER1.getId()));
//        Assert.assertNotNull(userService.removeOneById(USER1.getId()));
//        Assert.assertNull(userService.findOneById(USER1.getId()));
//
//        thrown.expect(IdEmptyException.class);
//        userService.removeOneById(NULL_USER_ID);
//    }
//
//    @Test
//    public void testUserRemoveOneByIndex() {
//        userService.add(USER1);
//        Assert.assertNotNull(userService.findOneById(USER1.getId()));
//        Assert.assertNotNull(userService.removeOneByIndex(0));
//        Assert.assertNull(userService.findOneById(USER1.getId()));
//
//        thrown.expect(IndexIncorrectException.class);
//        userService.removeOneByIndex(-1);
//    }
//
//    @Test
//    public void testUserRemoveAll() {
//        userService.add(USER_LIST);
//        Assert.assertEquals(USER_LIST.size(), userService.findAll().size());
//        userService.removeAll();
//        Assert.assertEquals(0, userService.findAll().size());
//    }
//
//    @Test
//    public void testUserGetSize() {
//        userService.add(USER_LIST);
//        Assert.assertEquals(USER_LIST.size(), userService.getSize());
//    }
//
//    @Test
//    public void testUserExistsById() {
//        userService.add(USER1);
//        Assert.assertTrue(userService.existsById(USER1.getId()));
//        Assert.assertFalse(userService.existsById(USER2.getId()));
//    }
//
//    @Test
//    public void testUserCreate() {
//        Assert.assertEquals(0, userService.findAll().size());
//        @NotNull final String name = "USER_TEST";
//        @NotNull final String password = "password";
//        Assert.assertNotNull(userService.create(name, password));
//        Assert.assertEquals(1, userService.findAll().size());
//        @NotNull final UserDTO user = userService.findOneByIndex(0);
//        Assert.assertNotNull(user);
//        Assert.assertEquals(name, user.getLogin());
//        Assert.assertEquals(HashUtil.salt(propertyService, password), user.getPasswordHash());
//
//        thrown.expect(LoginEmptyException.class);
//        userService.create(null, password);
//        thrown.expect(ExistsLoginException.class);
//        userService.create(name, password);
//        thrown.expect(PasswordEmptyException.class);
//        userService.create("EXCEPTION_NAME", null);
//    }
//
//    @Test
//    public void testUserCreateEmail() {
//        Assert.assertEquals(0, userService.findAll().size());
//        @NotNull final String name = "USER_TEST";
//        @NotNull final String password = "password";
//        @NotNull final String email = "user@test.ru";
//        Assert.assertNotNull(userService.create(name, password, email));
//        Assert.assertEquals(1, userService.findAll().size());
//        @NotNull final UserDTO user = userService.findOneByIndex(0);
//        Assert.assertNotNull(user);
//        Assert.assertEquals(name, user.getLogin());
//        Assert.assertEquals(HashUtil.salt(propertyService, password), user.getPasswordHash());
//        Assert.assertEquals(email, user.getEmail());
//
//        thrown.expect(LoginEmptyException.class);
//        userService.create(null, password, email);
//        thrown.expect(ExistsLoginException.class);
//        userService.create(name, password, email);
//        thrown.expect(PasswordEmptyException.class);
//        userService.create(name, null, email);
//        thrown.expect(ExistsEmailException.class);
//        userService.create("USER_TEST_02", password, email);
//    }
//
//    @Test
//    public void testUserCreateRole() {
//        Assert.assertEquals(0, userService.findAll().size());
//        @NotNull final String name = "USER_TEST";
//        @NotNull final String password = "password";
//        @NotNull final Role role = Role.ADMIN;
//        Assert.assertNotNull(userService.create(name, password, role));
//        Assert.assertEquals(1, userService.findAll().size());
//        @NotNull final UserDTO user = userService.findOneByIndex(0);
//        Assert.assertNotNull(user);
//        Assert.assertEquals(name, user.getLogin());
//        Assert.assertEquals(HashUtil.salt(propertyService, password), user.getPasswordHash());
//
//        thrown.expect(LoginEmptyException.class);
//        userService.create(null, password, role);
//        thrown.expect(ExistsLoginException.class);
//        userService.create(name, password, role);
//        thrown.expect(PasswordEmptyException.class);
//        userService.create(name, null, role);
//        thrown.expect(RoleEmptyException.class);
//        @Nullable Role roleNull = null;
//        userService.create(name, password, roleNull);
//    }
//
//    @Test
//    public void testUserFindByLogin() {
//        userService.add(USER1);
//        userService.add(ADMIN);
//        Assert.assertNull(userService.findByLogin(USER2.getLogin()));
//        Assert.assertEquals(USER1.getLogin(), userService.findByLogin(USER1.getLogin()).getLogin());
//
//        thrown.expect(LoginEmptyException.class);
//        userService.findByLogin(null);
//    }
//
//    @Test
//    public void testUserFindByEmail() {
//        userService.add(USER1);
//        userService.add(ADMIN);
//        Assert.assertNull(userService.findByEmail(USER2.getEmail()));
//        Assert.assertEquals(USER1.getEmail(), userService.findByEmail(USER1.getEmail()).getEmail());
//
//        thrown.expect(EmailEmptyException.class);
//        userService.findByEmail(null);
//    }
//
//    @Test
//    public void testUserRemoveOneByLogin() {
//        userService.add(USER1);
//        userService.add(USER2);
//        userService.add(ADMIN);
//        Assert.assertNotNull(userService.findByLogin(USER2.getLogin()));
//        Assert.assertNotNull(userService.removeOneByLogin(USER2.getLogin()));
//        Assert.assertNull(userService.findByLogin(USER2.getLogin()));
//
//        thrown.expect(LoginEmptyException.class);
//        userService.removeOneByLogin(null);
//        thrown.expect(EntityNotFoundException.class);
//        userService.removeOneByEmail("WRONG_LOGIN");
//        thrown.expect(Exception.class);
//    }
//
//    @Test
//    public void testUserRemoveOneByEmail() {
//        userService.add(USER1);
//        userService.add(USER2);
//        userService.add(ADMIN);
//        Assert.assertNotNull(userService.findByEmail(USER2.getEmail()));
//        Assert.assertNotNull(userService.removeOneByEmail(USER2.getEmail()));
//        Assert.assertNull(userService.findByEmail(USER2.getEmail()));
//
//        thrown.expect(EmailEmptyException.class);
//        userService.removeOneByEmail(null);
//        thrown.expect(EntityNotFoundException.class);
//        userService.removeOneByEmail("WRONG_EMAIL");
//    }
//
//    @Test
//    public void testUserSetPassword() {
//        userService.add(USER1);
//        userService.add(USER2);
//        userService.add(ADMIN);
//        @NotNull UserDTO user = userService.findByLogin(USER2.getLogin());
//        Assert.assertEquals(USER2.getPasswordHash(), user.getPasswordHash());
//
//        @NotNull final String newPassword = "NEW_PASSWORD";
//        user = userService.setPassword(user.getId(), newPassword);
//        Assert.assertEquals(USER2.getLogin(), user.getLogin());
//        Assert.assertEquals(HashUtil.salt(propertyService, newPassword), user.getPasswordHash());
//
//        user = userService.findByLogin(USER2.getLogin());
//        Assert.assertEquals(HashUtil.salt(propertyService, newPassword), user.getPasswordHash());
//
//        thrown.expect(IdEmptyException.class);
//        userService.setPassword(null, newPassword);
//        thrown.expect(PasswordEmptyException.class);
//        userService.setPassword(user.getId(), null);
//        thrown.expect(EntityNotFoundException.class);
//        userService.setPassword("WRONG_ID", newPassword);
//    }
//
//    @Test
//    public void testUserUpdateUser() {
//        userService.add(USER1);
//        userService.add(USER2);
//        userService.add(ADMIN);
//        @NotNull UserDTO user = userService.findByLogin(USER2.getLogin());
//
//        @NotNull final String firstName = "FIRST_NAME";
//        @NotNull final String lastName = "LAST_NAME";
//        @NotNull final String middleName = "MIDDLE_NAME";
//        user = userService.updateUser(user.getId(), firstName, lastName, middleName);
//        Assert.assertEquals(firstName, user.getFirstName());
//        Assert.assertEquals(lastName, user.getLastName());
//        Assert.assertEquals(middleName, user.getMiddleName());
//
//        user = userService.findByLogin(USER2.getLogin());
//        Assert.assertEquals(firstName, user.getFirstName());
//        Assert.assertEquals(lastName, user.getLastName());
//        Assert.assertEquals(middleName, user.getMiddleName());
//
//        thrown.expect(IdEmptyException.class);
//        userService.updateUser(null, firstName, lastName, middleName);
//        thrown.expect(EntityNotFoundException.class);
//        userService.updateUser("WRONG_ID", firstName, lastName, middleName);
//    }
//
//    @Test
//    public void testUserLockOneByLogin() {
//        userService.add(USER1);
//        userService.add(USER2);
//        userService.add(ADMIN);
//        @NotNull UserDTO user = userService.findByLogin(USER2.getLogin());
//        Assert.assertEquals(false, user.getLocked());
//
//        user = userService.lockOneByLogin(USER2.getLogin());
//        Assert.assertNotNull(user);
//        Assert.assertEquals(true, user.getLocked());
//
//        user = userService.findByLogin(USER2.getLogin());
//        Assert.assertNotNull(user);
//        Assert.assertEquals(true, user.getLocked());
//
//        thrown.expect(LoginEmptyException.class);
//        userService.lockOneByLogin(null);
//        thrown.expect(UserNotFoundException.class);
//        userService.lockOneByLogin("WRONG_LOGIN");
//    }
//
//    @Test
//    public void testUserUnlockOneByLogin() {
//        @NotNull final UserDTO lockUser = new UserDTO("USER_LOCK", "user_lock", "user_lock@address.ru");
//        lockUser.setLocked(true);
//        userService.add(USER1);
//        userService.add(USER2);
//        userService.add(lockUser);
//        userService.add(ADMIN);
//        @NotNull UserDTO user = userService.findByLogin(lockUser.getLogin());
//        Assert.assertEquals(true, user.getLocked());
//
//        user = userService.unlockOneByLogin(lockUser.getLogin());
//        Assert.assertNotNull(user);
//        Assert.assertEquals(false, user.getLocked());
//
//        user = userService.findByLogin(lockUser.getLogin());
//        Assert.assertNotNull(user);
//        Assert.assertEquals(false, user.getLocked());
//
//        thrown.expect(LoginEmptyException.class);
//        userService.unlockOneByLogin(null);
//        thrown.expect(UserNotFoundException.class);
//        userService.unlockOneByLogin("WRONG_LOGIN");
//    }
//
//    @Test
//    public void testUserIsLoginExist() {
//        userService.add(USER1);
//        userService.add(ADMIN);
//        Assert.assertFalse(userService.isLoginExist(USER2.getLogin()));
//        Assert.assertTrue(userService.isLoginExist(USER1.getLogin()));
//    }
//
//    @Test
//    public void testUserIsEmailExist() {
//        userService.add(USER1);
//        userService.add(ADMIN);
//        Assert.assertFalse(userService.isEmailExist(USER2.getEmail()));
//        Assert.assertTrue(userService.isEmailExist(USER1.getEmail()));
//    }

}
