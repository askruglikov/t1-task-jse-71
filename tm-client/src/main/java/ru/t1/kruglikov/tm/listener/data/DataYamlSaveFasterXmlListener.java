package ru.t1.kruglikov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.data.DataYamlSaveFasterXmlRequest;
import ru.t1.kruglikov.tm.event.ConsoleEvent;


@Component
public final class DataYamlSaveFasterXmlListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-save-yaml";

    @NotNull
    public static final String DESCRIPTION = "Save data in yaml file.";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataYamlSaveFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA SAVE YAML]");

        domainEndpoint.yamlSaveFasterXml(new DataYamlSaveFasterXmlRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
