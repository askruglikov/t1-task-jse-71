package ru.t1.kruglikov.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.kruglikov.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.kruglikov.tm.enumerated.Role;
import ru.t1.kruglikov.tm.event.ConsoleEvent;
import ru.t1.kruglikov.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordListener extends AbstractUserListener {

    @NotNull private final String NAME = "user-change-password";
    @NotNull private final String DESCRIPTION = "Change password.";

    @Override
    @EventListener(condition = "@userChangePasswordListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER CHANGE PASSWORD]");

        System.out.println("Enter new password:");
        @Nullable final String password = TerminalUtil.nextLine();

        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setPassword(password);
        userEndpoint.changePassword(request);
    }

    @NotNull @Override
    public String getName() {
        return NAME;
    }

    @NotNull @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
