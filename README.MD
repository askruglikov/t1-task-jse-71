# T1-TASK-JSE

## DEVELOPER INFO

**NAME**: Andrey Kruglikov

**EMAIL**: akruglikov@t1-consulting.ru

## SOFTWARE

**JAVA**: OPENJDK 1.8

**OS**: Windows 10

## HARDWARE

**CPU**: AMD Ryzen 7

**RAM**: 16GB

**SSD**: 512GB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./t1-task-jse.jar
```