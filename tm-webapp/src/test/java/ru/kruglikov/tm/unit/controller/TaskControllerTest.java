package ru.kruglikov.tm.unit.controller;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.kruglikov.tm.config.ApplicationConfiguration;
import ru.kruglikov.tm.config.DataBaseConfiguration;
import ru.kruglikov.tm.config.WebApplicationConfiguration;
import ru.kruglikov.tm.marker.UnitCategory;
import ru.kruglikov.tm.model.Task;
import ru.kruglikov.tm.service.TaskService;
import ru.kruglikov.tm.util.UserUtil;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationConfiguration.class,
        DataBaseConfiguration.class,
        WebApplicationConfiguration.class
})
public class TaskControllerTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";
    @NotNull
    private final Task task1 = new Task(UUID.randomUUID().toString());
    @NotNull
    private final Task task2 = new Task(UUID.randomUUID().toString());
    @Autowired
    @NotNull
    private TaskService taskService;
    @Autowired
    @NotNull
    private AuthenticationManager authenticationManager;
    @NotNull
    private MockMvc mockMvc;
    @Autowired
    @NotNull
    private WebApplicationContext wac;

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskService.addByUserId(task1, UserUtil.getUserId());
        taskService.addByUserId(task2, UserUtil.getUserId());
    }

    @After
    public void clean() {
        taskService.removeAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void findAllTest() {
        @NotNull final String url = "/tasks";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void createTest() {
        @NotNull final String url = "/task/create";
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        @NotNull final List<Task> projects = taskService.findAllByUserId(UserUtil.getUserId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(3, projects.size());
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull final String url = "/task/delete/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is3xxRedirection());
        Assert.assertNull(taskService.findOneByIdAndUserId(task1.getId(), UserUtil.getUserId()));
    }

    @Test
    @SneakyThrows
    public void editTest() {
        @NotNull final String url = "/task/edit/" + task1.getId();
        mockMvc.perform(MockMvcRequestBuilders.get(url))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

}
