package ru.kruglikov.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.kruglikov.tm.config.WebApplicationConfiguration;
import ru.kruglikov.tm.marker.UnitCategory;
import ru.kruglikov.tm.model.Task;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        WebApplicationConfiguration.class
})
public class TaskEndpointTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    final static String BASE_URL = "http://localhost:8081/api/tasks/";

    @NotNull
    private final Task task1 = new Task("Test Task1");

    @NotNull
    private final Task task2 = new Task("Test Task2");

    @NotNull
    private final Task task3 = new Task("Test Task3");

    @NotNull
    private final Task task4 = new Task("Test Task4");

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        save(task3);
        save(task4);
    }

    @SneakyThrows
    private void save(@NotNull final Task task) {
        @NotNull String url = BASE_URL + "save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private Task findById(@NotNull final String id) {
        @NotNull String url = BASE_URL + "findById/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if (json.equals("")) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, Task.class);
    }

    @SneakyThrows
    private List<Task> findAll() {
        @NotNull String url = BASE_URL + "findAll";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, Task[].class));
    }

    @Test
    @SneakyThrows
    public void contextLoadsTest() {
        Assert.assertNotNull(authenticationManager);
        Assert.assertNotNull(wac);
        Assert.assertNotNull(mockMvc);
    }

    @Test
    public void findByIdTest() {
        save(task1);
        final Task task = findById(task1.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), task1.getId());
    }

    @Test
    public void saveTest() {
        save(task2);
        @Nullable final Task task = findById(task2.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task2.getId(), task.getId());
    }

    @Test
    @SneakyThrows
    public void deleteByIdTest() {
        @NotNull String url = BASE_URL + "deleteById/" + task3.getId();
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(task3.getId()));
    }

    @Test
    @SneakyThrows
    public void deleteTest() {
        @NotNull String url = BASE_URL + "delete";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(task4);
        mockMvc.perform(MockMvcRequestBuilders.post(url).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertNull(findById(task4.getId()));
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, findAll().size());
    }

}