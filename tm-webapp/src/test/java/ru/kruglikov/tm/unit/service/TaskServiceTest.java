package ru.kruglikov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.kruglikov.tm.config.WebApplicationConfiguration;
import ru.kruglikov.tm.marker.UnitCategory;
import ru.kruglikov.tm.model.Task;
import ru.kruglikov.tm.service.TaskService;
import ru.kruglikov.tm.util.UserUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        WebApplicationConfiguration.class
})
public class TaskServiceTest {

    @NotNull
    private static final String USERNAME = "test";

    @NotNull
    private static final String USERPASSWORD = "test";

    @NotNull
    private final Task task1 = new Task("Test Task1");

    @NotNull
    private final Task task2 = new Task("Test Task2");

    @NotNull
    private final Task task3 = new Task("Test Task3");

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private TaskService taskService;

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USERNAME, USERPASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        taskService.addByUserId(task1, UserUtil.getUserId());
        taskService.addByUserId(task2, UserUtil.getUserId());
    }

    @After
    public void cleanByUserIdTest() {
        taskService.removeAllByUserId(UserUtil.getUserId());
    }

    @Test
    public void findAllByUserIdTest() {
        Assert.assertEquals(2, taskService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void findByIdAndUserId() {
        Assert.assertNotNull(taskService.findOneByIdAndUserId(task1.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeByIdAndUserId() {
        @NotNull final List<Task> tasks = new ArrayList<>();
        tasks.add(task1);
        taskService.removeByIdAndUserId(task1.getId(), UserUtil.getUserId());
        Assert.assertNull(taskService.findOneByIdAndUserId(task1.getId(), UserUtil.getUserId()));
    }

    @Test
    public void removeAllByUserId() {
        taskService.removeAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskService.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    public void writeList() {
        taskService.add(task3);
        Assert.assertEquals(task3.getName(), taskService.findOneById(task3.getId()).getName());
        task3.setName(UUID.randomUUID().toString());
        taskService.update(task3);
        Assert.assertEquals(task3.getName(), taskService.findOneById(task3.getId()).getName());
    }

}