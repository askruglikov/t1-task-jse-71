package ru.kruglikov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.kruglikov.tm.api.endpoint.IAuthEndpoint;
import ru.kruglikov.tm.model.Result;
import ru.kruglikov.tm.model.User;
import ru.kruglikov.tm.service.UserService;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/auth")
@WebService(endpointInterface = "ru.kruglikov.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint implements IAuthEndpoint {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserService userService;

    @Override
    @WebMethod
    @PostMapping("/login")
    public Result login(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (final Exception e) {
            return new Result(e);
        }
    }

    @Override
    @WebMethod
    @GetMapping("/profile")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public User profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final String login = authentication.getName();
        return userService.findByLogin(login);
    }

    @Override
    @PostMapping("/logout")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}