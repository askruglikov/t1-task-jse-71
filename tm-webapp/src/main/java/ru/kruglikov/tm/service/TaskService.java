package ru.kruglikov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kruglikov.tm.api.repository.ITaskRepository;
import ru.kruglikov.tm.exception.user.AccessDeniedException;
import ru.kruglikov.tm.model.Task;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Transactional
    public void add(@Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Transactional
    public void addByUserId(@Nullable final Task model, @Nullable final String userId) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        model.setUserId(userId);
        taskRepository.save(model);
    }

    @Transactional
    public void update(@Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.save(model);
    }

    @Nullable
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Nullable
    public List<Task> findAllByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return taskRepository.findAllByUserId(userId);
    }

    @Transactional
    public void remove(@Nullable Task model) {
        if (model == null) throw new EntityNotFoundException();
        taskRepository.delete(model);
    }

    @Transactional
    public void removeByUserId(@Nullable final Task model, @Nullable final String userId) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        taskRepository.deleteByIdAndUserId(model.getId(), userId);
    }

    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        taskRepository.deleteById(id);
    }

    @Transactional
    public void removeByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        taskRepository.deleteByIdAndUserId(id, userId);
    }

    @Transactional
    public void removeAllByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        taskRepository.deleteAllByUserId(userId);
    }

    @Nullable
    public Task findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        return taskRepository.findById(id).orElse(null);
    }

    @Nullable
    public Task findOneByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        return taskRepository.findByIdAndUserId(id, userId);
    }

}