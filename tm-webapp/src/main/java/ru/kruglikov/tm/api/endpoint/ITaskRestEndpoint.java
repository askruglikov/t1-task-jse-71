package ru.kruglikov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.kruglikov.tm.model.Project;
import ru.kruglikov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("api/tasks")
public interface ITaskRestEndpoint {

    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    Collection<Task> findAll();

    @Nullable
    @WebMethod
    @GetMapping("findById/{id}")
    Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @NotNull
    @WebMethod
    @PostMapping("/save")
    Task save(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    );

    @WebMethod
    @PostMapping("delete/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll();

}