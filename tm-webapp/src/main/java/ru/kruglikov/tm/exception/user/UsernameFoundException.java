package ru.kruglikov.tm.exception.user;

public class UsernameFoundException extends AbstractUserException{

    public UsernameFoundException() {
        super("Error! Permission is incorrect...");
    }

}
