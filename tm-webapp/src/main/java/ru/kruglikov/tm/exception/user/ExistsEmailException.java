package ru.kruglikov.tm.exception.user;

import org.jetbrains.annotations.NotNull;

public final class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email alredy exists...");
    }

    public ExistsEmailException(@NotNull String email) {
        super("Error! Email '" + email + "'alredy exists...");
    }

}
