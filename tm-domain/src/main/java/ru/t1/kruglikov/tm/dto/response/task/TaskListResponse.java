package ru.t1.kruglikov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.TaskDTO;
import ru.t1.kruglikov.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskListResponse extends AbstractTaskResponse {

    @Nullable
    private List<TaskDTO> tasks;

}
