package ru.t1.kruglikov.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.model.TaskDTO;
import ru.t1.kruglikov.tm.dto.response.AbstractResponse;
import ru.t1.kruglikov.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private TaskDTO task;

    public AbstractTaskResponse(@Nullable TaskDTO task) {
        this.task = task;
    }

}
