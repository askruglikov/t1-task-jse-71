package ru.t1.kruglikov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kruglikov.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectCompleteByIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public ProjectCompleteByIdRequest(@Nullable String token) {
        super(token);
    }

}
